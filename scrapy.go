package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

var errNoProductsFound = errors.New("no products were found")

const path string = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html"

// RipeFruit is one item found on the results page
type RipeFruit struct {
	Title       string `json:"title,omitempty"`
	Size        string `json:"size,omitempty"`
	UnitPrice   string `json:"unit_price,omitempty"`
	Description string `json:"description,omitempty"`
}

// newRipeFruit is a convenience function to create a new result item
func newRipeFruit(title, size, description string, unitPrice string) *RipeFruit {
	return &RipeFruit{title, size, unitPrice, description}
}

// Results is a collection of RipeFruit
type Results []*RipeFruit

// getProductDetails returns all the product page size, and its description
func getProductDetails(productURL, title, pricePerUnit string, c chan *RipeFruit, e chan error) {
	doc, err := goquery.NewDocument(productURL)
	if err != nil {
		e <- err
		return
	}

	html, err := doc.Html()
	if err != nil {
		e <- err
		return
	}

	var size = len(html) / 1024
	description, _ := doc.Find("meta[name=\"description\"]").First().Attr("content")
	c <- newRipeFruit(title, fmt.Sprintf("%.1fkb", float32(size)), description, pricePerUnit)
}

// getResultsForURL returns an array of all the product items
func getResultsForURL(url string) (Results, error) {
	doc, err := goquery.NewDocument(url)
	if err != nil {
		return nil, err
	}

	// find all the products on the page
	products := doc.Find(".productLister .product")
	if products.Length() == 0 {
		return nil, errNoProductsFound
	}

	c := make(chan *RipeFruit)
	e := make(chan error)

	products.Each(func(i int, s *goquery.Selection) {
		title := strings.TrimSpace(s.Find("h3").Text())
		pricePerUnit := strings.TrimSpace(s.Find(".pricePerUnit").Text())
		productURL, exists := s.Find(".productInfo a").Attr("href")
		if !exists {
			c <- newRipeFruit(title, "", "", pricePerUnit)
			return
		}
		go getProductDetails(strings.TrimSpace(productURL), title, pricePerUnit, c, e)
	})

	var results Results
	for i := 0; i < products.Length(); i++ {
		select {
		case fruit := <-c:
			results = append(results, fruit)
		case err := <-e:
			return nil, err
		}
	}

	return results, nil
}

func main() {
	results, err := getResultsForURL(path)
	if err != nil {
		panic(err)
	}

	rawResults, err := json.MarshalIndent(results, "", "    ")
	if err != nil {
		panic(err)
	}

	fmt.Println(string(rawResults))
}
