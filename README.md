# scrapy

Clone the app into your $GOPATH. Make sure you have GO15VENDOREXPERIMENT enabled

# Run

`$ go run scrapy.go`

# Test

`$ go test -v`