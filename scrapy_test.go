package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func fileWrapperHandler(file string, status int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(status)
		b, err := ioutil.ReadFile(file)
		if err != nil {
			panic(err)
		}
		w.Write(b)
	}
}

var (
	mux    *http.ServeMux
	server *httptest.Server
)

func setup() {
	mux = http.NewServeMux()
	mux.HandleFunc("/index.html", fileWrapperHandler("./fixtures/index.html", http.StatusOK))
	server = httptest.NewServer(mux)
}

func teardown() {
	server.Close()
}

func TestGetResultsForURL(t *testing.T) {
	setup()
	results, err := getResultsForURL(server.URL + "/index.html")
	if err != nil {
		t.Fatal(err)
	}

	if got, want := len(results), 7; got != want {
		t.Errorf("Got %q, want %q", got, want)
	}
}
